#include "AAOscPW.h"
#include "AAIntrinsics.h"



#define kMinPW 0.01
#define kMaxPW 0.99

using namespace AADsp;


    
#pragma mark Creators and Destroyers
    
AAOscPW::AAOscPW (ushort tableSize, uint sampleRate) : AAOsc(tableSize, sampleRate) {
    // init tables to null
    tableSquareUp = NULL;
    tableSquareDn = NULL;
    
    setPW(0.5); // must call this to prep curvPhase's
    
    // shift forward by 90 degrees to start in the correct phase.  So the first val is 0
    osc->tickInfo->curvPhase += static_cast<uint>(0.25 * 4294967296.0);
    curvPhase2 += static_cast<uint>(0.25 * 4294967296.0);
}


AAOscPW::~AAOscPW() {
    if (tableSquareUp) aA_SSE_freeAligned(tableSquareUp);
    if (tableSquareDn) aA_SSE_freeAligned(tableSquareDn);
}


int AAOscPW::initOscPW(ushort harmSquarePW) {
    if (harmSquarePW == 0) harmSquarePW = 10;
    
    int err = 0;
    
    if (buildWaveSquarePW(harmSquarePW)) err++;
    
    setType(tOscTypePW_Square);
    
    return err;
}


void AAOscPW::initSigVector(const uint maxLength) {
    uint maxLengthOverrun = maxLength % 4;
    
    // make sure the length of the signal vector is a multiple of 4
    maxLengthSigVector = maxLength;
    if (maxLengthOverrun != 0)
        maxLengthSigVector += (4 - maxLengthOverrun);
    
    if (phaseArray1) aA_SSE_freeAligned(phaseArray1);
    if (phaseArray2) aA_SSE_freeAligned(phaseArray2);
    
    phaseArray1 = (float*) aA_SSE_mallocAligned(sizeof(float) * maxLengthSigVector);
    phaseArray2 = (float*) aA_SSE_mallocAligned(sizeof(float) * maxLengthSigVector);
}



#pragma mark - Builders


int AAOscPW::buildWaveSquarePW(ushort harms) {
    int err = 0;
    
    if (harms >= osc->tableSize / 2.0) {
        puts("ERROR: harms must be < the tableSize / 2");
        err++;
        return err;
    }
    
    // malloc 2 tables each with 2 guard points.
    tableSquareUp = (float*) aA_SSE_mallocAligned((osc->tableSize + 2) * sizeof(float));
    if (!tableSquareUp) {
        puts("ERROR: unable to alloc table for buildWaveSawDown");
        err++;
        return err;
    }
    
    tableSquareDn = (float*) aA_SSE_mallocAligned((osc->tableSize + 2) * sizeof(float));
    if (!tableSquareDn) {
        puts("ERROR: unable to alloc table for buildWaveSawDown");
        err++;
        return err;
    }

    const float step = static_cast<float>(TWOPI / osc->tableSize);
    float valUp;
    float valDn;
    ushort harmonic=1;
    
    // clear each table
    for (int i=0; i<osc->tableSize; i++) {
        tableSquareUp[i] = 0;
        tableSquareDn[i] = 0;
    }
    
    // now create each table simultaneously
    for (int i=0; i<harms; i++, harmonic++) {
        valUp = static_cast<float>(-1.0 / harmonic);
        valDn = static_cast<float>(1.0 / harmonic);
        
        for (int j=0; j<osc->tableSize; j++) {
            const float temp = sinf(step * harmonic * j);
            tableSquareUp[j] += valUp * temp;
            tableSquareDn[j] += valDn * temp;
        }
    }
    
    // norm the tables
    normTable(tableSquareUp);
    normTable(tableSquareDn);
    
    return err;
}



#pragma mark - Setters

void AAOscPW::setPW(float pulseWidth) {
    if (pw == pulseWidth) return;
    
    if (pulseWidth < kMinPW) pulseWidth = kMinPW;
    else if (pulseWidth > kMaxPW) pulseWidth = kMaxPW;
    
    const uint prevOff = static_cast<uint>((4294967296.0 * pw) * 0.5);
    pw = pulseWidth;
    
    const uint offset = static_cast<uint>((4294967296.0 * pw) * 0.5) - prevOff;
    
    osc->tickInfo->curvPhase += offset;
    curvPhase2 -= offset;
}




#pragma mark - Utility







#pragma mark - Generators


float AAOscPW::genTruncate() {
    const uint indexUp = static_cast<uint>(osc->tickInfo->curvPhase >> fracBits);
    const uint indexDn = static_cast<uint>(curvPhase2 >> fracBits);
    
    osc->tickInfo->curvPhase += osc->tickInfo->increment;
    curvPhase2 += osc->tickInfo->increment;
    
    return ((tableSquareUp[indexUp]*0.5)+(tableSquareDn[indexDn]*0.5)) * osc->amp;
}


float AAOscPW::genInterpLinear() {
    const uint iBase1 = static_cast<uint>(osc->tickInfo->curvPhase >> fracBits);
    const uint iNext1 = iBase1 + 1;
    const uint iBase2 = static_cast<uint>(curvPhase2 >> fracBits);
    const uint iNext2 = iBase2 + 1;
    
    const double frac1  = static_cast<double>((osc->tickInfo->curvPhase >> fracBits) - iBase1);
    const double slope1 = static_cast<double>(tableSquareUp[iNext1] - tableSquareUp[iBase1]);
    const double frac2  = static_cast<double>((curvPhase2 >> fracBits) - iBase2);
    const double slope2 = static_cast<double>(tableSquareDn[iNext2] - tableSquareDn[iBase2]);
    
    osc->tickInfo->curvPhase += osc->tickInfo->increment;
    curvPhase2 += osc->tickInfo->increment;
    
    return ((((frac1 * slope1) + tableSquareUp[iBase1]) / 2.0) + (((frac2 * slope2) + tableSquareDn[iBase2]) / 2.0)) * osc->amp;

}


void AAOscPW::genSSE_Linear(float *outSignal, uint length) {
//    float phaseArray1[length];
//    float phaseArray2[length];
//    float *tempArray1 = (float*) aA_SSE_mallocAligned(sizeof(float)*length);
//    float *tempArray2 = (float*) aA_SSE_mallocAligned(sizeof(float)*length);
//    
//    for (int i=0; i<length; i++) {
//        phaseArray1[i] = static_cast<float>(osc->tickInfo->curvPhase >> fracBits);
//        phaseArray2[i] = static_cast<float>(curvPhase2 >> fracBits);
//        
//        osc->tickInfo->curvPhase += osc->tickInfo->increment;
//        curvPhase2 += osc->tickInfo->increment;
//    }
    
    assert(length <= maxLengthSigVector);
    
    for (int i=0; i<length; i++) {
        phaseArray1[i] = static_cast<float>(osc->tickInfo->curvPhase >> fracBits);
        phaseArray2[i] = static_cast<float>(curvPhase2 >> fracBits);
        
        osc->tickInfo->curvPhase += osc->tickInfo->increment;
        curvPhase2 += osc->tickInfo->increment;
    }
    
    uint lengthOverrun = length % 4;
    if (lengthOverrun != 0)
        length += (4 - lengthOverrun);
    
    assert(length <= maxLengthSigVector);
    
    float *tempArray1 = (float*) aA_SSE_mallocAligned(sizeof(float)*length);
    float *tempArray2 = (float*) aA_SSE_mallocAligned(sizeof(float)*length);
    
    aA_SSE_vInterpLinear(phaseArray1, tableSquareUp, tempArray1, length);
    aA_SSE_vInterpLinear(phaseArray2, tableSquareDn, tempArray2, length);
    
    // mult
    aA_SSE_vScale(tempArray1, 0.5, tempArray1, length);
    aA_SSE_vScale(tempArray2, 0.5, tempArray2, length);
    
    // add
    aA_SSE_vAdd(tempArray1, tempArray2, outSignal, length);
    
    // scale
    aA_SSE_vScale(outSignal, osc->amp, outSignal, length);
    
    aA_SSE_freeAligned(tempArray1);
    aA_SSE_freeAligned(tempArray2);
}

































