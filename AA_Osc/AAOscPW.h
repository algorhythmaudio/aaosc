//  AAOsc.h
//  AA_Osc
//
//  Created by GW on 2012/12/16.
//  Copyright (c) 2012 Algorhythm_Audio. All rights reserved.
//  Last Updated: 2013/1/27
//
//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AAOscPW - a wavetable synthesis oscillator class with adjustable pulse width
//
// @Usage
//      - Similar to its parent class AAOsc in creation and usage.
//      - call setPW() to change and automate the pulse-width
//
// @Notes
//      - Keep the harmonics high, it helps with keeping the wave square like.  The default is
//        set to 60.
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#ifndef __AA_Osc__AAOscPW__
#define __AA_Osc__AAOscPW__


#include "AAOsc.h"

namespace AADsp {
    
    
#ifndef _OscTypePW_
#define _OscTypePW_
    
    typedef enum {
        tOscTypePW_Square,
        tOscTypePW_All,
        
        tOscTypePW_Invalid = -2
    } OscTypePW;
    
#endif

    

class AAOscPW : public AAOsc {
    
// instance vaiables
public:
    
    float       pw = 0.0; // set to 0 to prevent bug
    
    
private:
    
    // a square wave with pw, requires 2 inverse sawtooth waves
    float       *tableSquareUp;
    float       *tableSquareDn;
    
    // used for gen
    uint        curvPhase2;
    
    // for genSSELinear()
    float           *phaseArray1, *phaseArray2;
    uint            maxLengthSigVector;
    
    
// functions
    
public:
    
    AAOscPW             (ushort tableSize, uint sampleRate);
    ~AAOscPW();
    
    // must call init
    int                 initOscPW       (ushort harmsSquarePW=10);

    // if using genSSE_Linear, you must call this function
    virtual void        initSigVector       (const uint maxLength);
    
    // setters
    void                setType         (OscTypePW type) {return;} // no need for this yet
    void                setPhase        (double phase) {return;} // not possible with pw
    void                setPW           (float pulseWidth);
    
    // getters
    double              oscPhase()      {return 0.0;} // can't really return a phase for this class.
    float               oscPW()         {return pw;}
    
    // generators, overridden from AAOsc
    virtual float       genTruncate();
    virtual float       genInterpLinear();
    
    //
    virtual void        genSSE_Linear   (float *outSignal, uint length);
    
    
    //virtual void        printTable(); // This is just for debugging
    

private:
    
    // creation wavetable functions for each wave type
    int                 buildWaveSquarePW   (ushort harms);
    
    // utility
};








} // AADsp namespace
#endif /* defined(__AA_Osc__AAOscPW__) */















