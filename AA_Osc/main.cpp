#include <iostream>
#include <time.h>

#include "AAOsc.h"
#include "AAOscPW.h"
#include "portsf.h"

#define kSampleRate 44100
#define kNumFrames 1024
#define kFreq 440.0
#define kLength 5
#define kNumCh 1
#define kOutfile "testingOsc.aif"

#define kTableSizeInBits 10
#define kPW 0.0

#define kNumOsc 100

using namespace AADsp;

int main(int argc, const char * argv[]) {
    FILE            *fp = NULL;
    PSF_PROPS       outprops;
    psf_format      outformat = PSF_FMT_UNKNOWN;
    
    double          dur, srate;
    
    int             i, err = 0;
    int             ofd = -1;
    unsigned long   nbufs, outframes, remainder;
    long            nframes = kNumFrames;
    float           *frame = NULL;
    clock_t         starttime, endtime;
    
    AAOsc *oscillators[kNumOsc];
    
    starttime = clock();
    
    for (int i=0; i<kNumOsc; i++) {
        oscillators[i] = new AAOsc(kTableSizeInBits, kSampleRate);
        if (oscillators[i]->initOsc(NULL, 0) != 0) {
            printf("Error: could init osc %i\n", i);
            return i;
        }
        
        // init vector
        oscillators[i]->initSigVector(kNumFrames);
    }
        
    endtime = clock();
    printf("Init time = %f secs\n", (endtime-starttime) / (double)CLOCKS_PER_SEC);
    
    for (int i=0; i<kNumOsc; i++) {
        oscillators[i]->setAmp(0.8);
        oscillators[i]->setFreq(kFreq);
        oscillators[i]->setType(tOscType_Sine);
    }
    
    
    

    
    
    // set and check arguments
    dur = kLength;
    if (dur <= 0.0) {
        puts("dur must be positive");
        return 1;
    }
    
    srate = kSampleRate;
    if (srate <= 0) {
        puts("srate must be positive");
        return 1;
    }
    
        
    
    outprops.chans = kNumCh;
    if (outprops.chans <= 0) {
        puts("chs must be positive");
        return 1;
    }
    
    
    
    
    
    
    
    
    // open, check, and prep outfile
    fp = fopen(kOutfile, "w");
    if (fp == NULL) {
        printf("Unable to open outfile: %s\n", kOutfile);
        err++;
        goto myexit;
    }
    
    if (psf_init()) {
        puts("Unable to start psf");
        err++;
        goto myexit;
    };
    
    outprops.srate = srate;
    outprops.samptype = (psf_stype) PSF_SAMP_32;
    outprops.chformat = STDWAVE;
    
    outformat = psf_getFormatExt(kOutfile);
    if (outformat == PSF_FMT_UNKNOWN) {
        printf("outfile name %s has unknown format.\n"
               "Use any of .wav, .aiff, .aif, .afc, .aifc\n", kOutfile);
        err++;
        goto myexit;
    }
    outprops.format = outformat;
    
    ofd = psf_sndCreate(kOutfile, &outprops, 0, 0, PSF_CREATE_RDWR);
    if (ofd < 0) {
        printf("Error: unable to create outfile %s\n", kOutfile);
        err++;
        goto myexit;
    }
    
    
    
    
    
    
    
    // set variables and alloc memory
    outframes = (unsigned long) (dur * outprops.srate + 0.5);
    nbufs = outframes / nframes;
    remainder = outframes - nbufs * nframes;
    if (remainder > 0) nbufs++;
    
    frame = (float*) malloc(nframes * outprops.chans * sizeof(float));
    if (frame == NULL) {
        puts("Couldn't alloc memory for frame");
        err++;
        goto myexit;
    }
    
    
    
    
    // *****************proc loop******************
    puts("Processing...\n");
    starttime = clock();
    
    for (i=0; i<nbufs; i++) {
        long j;
        double val;
        int x = 0, nch;
        
        if (i == nbufs-1) nframes = remainder;
        
        // synthesis
        oscillators[0]->genSSE_Linear(frame, nframes);
        
//        for (j=0; j<nframes; j++) {
//            // do synthesis!!!
//            
//            // set amp for each channel into the frame
//            for (nch=0; nch<outprops.chans; nch++)
//                frame[x++] = (float) val;
//        }
        
        //oscillators[kNumOsc-1]->setFreq(oscillators[kNumOsc-1]->oscFreq()+50);
        //oscPW->setPW(oscPW->oscPW() + 0.002);
        
        // write frame to file.
        if (psf_sndWriteFloatFrames(ofd, frame, nframes) != nframes) {
            puts("Error writing to outfile");
            err++;
            break;
            
        }
    }
    
    endtime = clock();
    
    printf("Completeq with %d errors\n", err);
    printf("Elapsed time = %f secs\n", (endtime-starttime) / (double)CLOCKS_PER_SEC);
    
    
    
    
        
    
myexit:
    if (ofd >= 0)
        if (psf_sndClose(ofd))
            printf("Error closing outfile %s\n", kOutfile);
       
    if (fp)         fclose(fp);
    if (ofd>0)      psf_sndClose(ofd);
    if (frame)      free(frame);
    //if (newOsc)     delete newOsc;
    
    for (int i=0; i<kNumOsc; i++)
        if (oscillators[i]) delete oscillators[i];
    
    psf_finish();
    
    puts("done.");
    return err;

    
    
    
}






    




