#include "AAOsc.h"
#include "AAIntrinsics.h"



#pragma mark Creators and Destoryers

namespace AADsp {

// constructor
AAOsc :: AAOsc(ushort tableSizeInBits, uint sampleRate) {
    // init tables to null
    currentTable    = NULL;
    tableSine       = NULL;
    tableSawUp      = NULL;
    tableSawDn      = NULL;
    tableTri        = NULL;
    tableSquare     = NULL;
    
    phaseArray           = NULL;
    maxLengthSigVector   = 0;
    
    if (tableSizeInBits > 31) {
        puts("Error: tableSizeInBits must be under 32.  Now set to 31");
        tableSizeInBits = 31;
    }
    
    if (sampleRate < MIN_SAMPLE_RATE) {
        sampleRate = MIN_SAMPLE_RATE;
        printf("Sample rate was below allowed sample rate.  SR is now set to %.1f\n", MIN_SAMPLE_RATE);
    }
    
    // create a new OscDesc and check that it malloc
    osc = (OscDesc*) malloc(sizeof(OscDesc));
    if (osc == NULL) {
        puts("ERROR: unable to malloc newRef for initOsc");
        return;
    }
    
    // malloc the tick info
    osc->tickInfo = (OscTickInfo*) malloc(sizeof(OscTickInfo));
    if (osc->tickInfo == NULL) {
        puts("ERROR: unable to alloc tickInfo for initOsc");
        deallocOsc(osc);
        return;
    }
    
    // set starting variables
    sr = sampleRate;
    osc->amp = 0.0;
    osc->tableSize = 1 << tableSizeInBits;
    osc->sizeOverSr = static_cast<double>(osc->tableSize / sr);
    osc->tickInfo->increment = 0.0;
    osc->tickInfo->curvPhase = 0.0;
    setFreq(1.0);
    
    // set nyquist
    nyquist = static_cast<double>(sr / 2.0);
    
    // set bit constants
    fracBits = 32-tableSizeInBits;
    fracMask = (1 << fracBits)-1;
}


// destructor
AAOsc :: ~AAOsc() {
    deallocOsc(osc);
}

    
void AAOsc :: deallocOsc(OscDesc *ref) {
    if (ref->tickInfo)  free(ref->tickInfo);
    if (ref)            free(ref);
    
    if (tableSine)      aA_SSE_freeAligned(tableSine);
    if (tableSawUp)     aA_SSE_freeAligned(tableSawUp);
    if (tableSawDn)     aA_SSE_freeAligned(tableSawDn);
    if (tableTri)       aA_SSE_freeAligned(tableTri);
    if (tableSquare)    aA_SSE_freeAligned(tableSquare);
    

    if (phaseArray)  aA_SSE_freeAligned(phaseArray);

}



/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 @Function: 
    int initOsc
 @Usage:
    Creates and alloc's new OscDesc depending on the desired 
 
 @Notes:
    - Passing arraySize=0 will build all types. For this, just pass null for typesArray
    - typesArray is array with each OscTypes desired.  Set arraySize accordingly.
    - Each harms argument will default to 20.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

int AAOsc :: initOsc(const ushort *typesArray, const ushort arraySize,
                     ushort harmsSawUp, ushort harmsSawDn, ushort harmsTri, ushort harmsSquare, ushort harmsSquarePW)
{
    if (harmsSawUp == 0) harmsSawUp = 20;
    if (harmsSawDn == 0) harmsSawDn = 20;
    if (harmsTri == 0) harmsTri = 20;
    if (harmsSquare == 0) harmsSquare = 20;
    
    int err = 0;
    
    if (arraySize == 0) {
        if (buildWaveSine()) return 1;
        if (buildWaveSawUp(harmsSawUp)) return 2;
        if (buildWaveSawDown(harmsSawDn)) return 3;
        if (buildWaveTriangle(harmsTri)) return 4;
        if (buildWaveSquare(harmsSquare)) return 5;
        
        setType(tOscType_Sine);
    }
    else {
        for (int i=0; i<arraySize; i++) {
            switch (typesArray[i]) {
                case tOscType_Sine:     if (buildWaveSine()) err++;                 break;
                case tOscType_SawUp:    if (buildWaveSawUp(harmsSawUp)) err++;      break;
                case tOscType_SawDown:  if (buildWaveSawDown(harmsSawDn)) err++;    break;
                case tOscType_Triangle: if (buildWaveTriangle(harmsTri)) err++;     break;
                case tOscType_Square:   if (buildWaveSquare(harmsSquare)) err++;    break;

                default:
                    puts("Error: unknown OscType");
                    err++;
                    break;
            }
        }
        
        setType((OscType) typesArray[0]);
    }
    return 0;
}

    
void AAOsc :: initSigVector(const uint maxLength) {
    uint maxLengthOverrun = maxLength % 4;
    
    // make sure the length of the signal vector is a multiple of 4
    maxLengthSigVector = maxLength;
    if (maxLengthOverrun != 0)
        maxLengthSigVector += (4 - maxLengthOverrun);
    
    if (phaseArray)
        aA_SSE_freeAligned(phaseArray);
    
    phaseArray = (float*) aA_SSE_mallocAligned(sizeof(float) * maxLengthSigVector);
}






#pragma mark - Builders

/* Wavetable builder functions
 
 @Purpose
    To malloc memory for tables and fill each wavetable with a specific wave type.
 
*/


int AAOsc :: buildWaveSine() {
    int err = 0;
    if (osc == NULL) {err++; return err;}
    
    // malloc table with 2 guard points
    tableSine = (float*) aA_SSE_mallocAligned((osc->tableSize + 2) * sizeof(float));
    if (!tableSine) {
        puts("ERROR: unable to alloc table for buildWaveSine");
        err++;
        return err;
    }
    
    // populate wavetable for sine wave
    const float step = static_cast<float>(TWOPI / osc->tableSize);
    
    // create sine wave
    for (int i=0; i < osc->tableSize; i++)
        tableSine[i] = sinf(step*i);
    
    // set guard points
    tableSine[osc->tableSize] = tableSine[0];
    tableSine[osc->tableSize+1] = tableSine[1];
    
    return err;
}


int AAOsc :: buildWaveSawUp(ushort harms) {
    int err = 0;
    if (osc == NULL) {err++; return err;}
    
    if (harms >= osc->tableSize / 2) {
        puts("ERROR: harms must be < the tableSize / 2");
        err++;
        return err;
    }
    
    // malloc table with 2 guard points
    tableSawUp = (float*) aA_SSE_mallocAligned((osc->tableSize + 2) * sizeof(float));
    if (!tableSawUp) {
        puts("ERROR: unable to alloc table for buildWaveSawUp");
        err++;
        return err;
    }
    
    const float step = static_cast<float>(TWOPI / osc->tableSize);
    float val;
    uint harmonic = 1;
    
    // first clear table
    for (int i=0; i < osc->tableSize; i++)
        tableSawUp[i] = 0.0;
    
    // now create new table
    for (int i=0; i < harms; i++, harmonic++) {
        val = static_cast<float>(-1.0 / harmonic);
        
        for (int j=0; j < osc->tableSize; j++)
            tableSawUp[j] += val * sinf(step * harmonic * j);
    }
    normTable(tableSawUp);
    
    return err;
}


int AAOsc :: buildWaveSawDown(ushort harms) {
    int err = 0;
    if (osc == NULL) {err++; return err;}
    
    if (harms >= osc->tableSize / 2) {
        puts("ERROR: harms must be < the tableSize / 2");
        err++;
        return err;
    }
    
    // malloc table with 2 guard points
    tableSawDn = (float*) aA_SSE_mallocAligned((osc->tableSize + 2) * sizeof(float));
    if (!tableSawDn) {
        puts("ERROR: unable to alloc table for buildWaveSawDown");
        err++;
        return err;
    }
    
    const float step = static_cast<float>(TWOPI / osc->tableSize);
    float val;
    uint harmonic = 1;
    
    // first clear table
    for (int i=0; i < osc->tableSize; i++)
        tableSawDn[i] = 0.0;
    
    // now create new table
    for (int i=0; i < harms; i++, harmonic++) {
        val = static_cast<float>(1.0 / harmonic);
        
        for (int j=0; j < osc->tableSize; j++)
            tableSawDn[j] += val * sinf(step * harmonic * j);
    }
    normTable(tableSawDn);
    
    return err;
}


int AAOsc :: buildWaveTriangle(ushort harms) {
    int err = 0;
    if (osc == NULL) {err++; return err;}
    
    if (harms >= osc->tableSize / 2) {
        puts("ERROR: harms must be < the tableSize / 2");
        err++;
        return err;
    }
    
    // malloc table with 2 guard points
    tableTri = (float*) aA_SSE_mallocAligned((osc->tableSize + 2) * sizeof(float));
    if (!tableTri) {
        puts("ERROR: unable to alloc table for buildWaveTriangle");
        err++;
        return err;
    }
    
    const float step = static_cast<float>(TWOPI / osc->tableSize);
    float amp;
    uint harmonic = 1;
    
    // first clear table
    for (int i=0; i < osc->tableSize; i++)
        tableTri[i] = 0.0;
    
    for (int i=0; i < harms; i++, harmonic+=2) {
        amp = static_cast<float>(1.0 / (harmonic * harmonic));
        
        for (int j=0; j < osc->tableSize; j++)
            tableTri[j] += amp * cosf(step * harmonic * j);
    }
    normTable(tableTri);
    
    return err;
}


int AAOsc :: buildWaveSquare(ushort harms) {
    int err = 0;
    if (osc == NULL) {err++; return err;}
    
    if (harms >= osc->tableSize / 2) {
        puts("ERROR: harms must be < the tableSize / 2");
        err++;
        return err;
    }
    
    // malloc table with 2 guard points
    tableSquare = (float*) aA_SSE_mallocAligned((osc->tableSize + 2) * sizeof(float));
    if (!tableSquare) {
        puts("ERROR: unable to alloc table for buildWaveSquare");
        err++;
        return err;
    }
    
    const float step = static_cast<float>(TWOPI / osc->tableSize);
    float amp;
    uint harmonic = 1;
    
    // first clear table
    for (int i=0; i < osc->tableSize; i++)
        tableSquare[i] = 0.0;
    
    for (int i=0; i < harms; i++, harmonic+=2) {
        amp = static_cast<float>(1.0 / harmonic);
        
        for (int j=0; j < osc->tableSize; j++)
            tableSquare[j] += amp * sinf(step * harmonic * j);
    }
    normTable(tableSquare);
    
    return err;
}

    


#pragma mark - Utility


// this normalizes a table and sets the guard point
void AAOsc :: normTable(float *table) {
    const float maxAmp = static_cast<float>(1.0 / aA_SSE_vMax(table, osc->tableSize));
    
    // norm table
    aA_SSE_vScale(table, maxAmp, table, osc->tableSize);
    
    // set guard points
    table[osc->tableSize] = table[0];
    table[osc->tableSize+1] = table[1];
}


void AAOsc :: setIncrement() {
    osc->tickInfo->increment = static_cast<int>((osc->freq / sr) * 4294967296.0);
}


void AAOsc :: printTable() {
    for (int i=0; i < osc->tableSize; i++)
        printf("%i\t\t%f\n", i, currentTable[i]);
}

    





#pragma mark - Setters


void AAOsc :: setFreq(double freq) {
    if (freq < OscFreqMin) freq = OscFreqMin;
    else if (freq > nyquist) freq = nyquist;

    osc->freq = freq;
    setIncrement();
}




void AAOsc :: setAmp(double amp) {
    if (amp < 0.0) amp = 0;
    osc->amp = amp;
}
    

void AAOsc :: setType(OscType type) {
    switch (type) {
        case tOscType_Sine:     currentTable = tableSine;   break;
        case tOscType_SawUp:    currentTable = tableSawUp;  break;
        case tOscType_SawDown:  currentTable = tableSawDn;  break;
        case tOscType_Triangle: currentTable = tableTri;    break;
        case tOscType_Square:   currentTable = tableSquare; break;
        
        default:
            puts("ERROR: invailid OscType for setType");
            return;
    }
    osc->type = type;
    osc->tickInfo->curvPhase = 0.0;
}


void AAOsc :: setSampleRate(uint sampleRate) {
    if (sampleRate <= 0) {
        puts("ERROR: Sample rate must be positive for changeSampleRate");
        return;
    }
    
    sr = sampleRate;

    osc->sizeOverSr = osc->tableSize / sr;
    
    setIncrement();
    nyquist = static_cast<double>(sr * 0.5);
}


void AAOsc :: setPhase(double phase) {
    if (phase < 0.0) phase = 0.0;
    if (phase > 1.0) phase = 1.0;
    
    osc->tickInfo->curvPhase = static_cast<uint>(phase * 4294967296.0);
}

    
    
    
#pragma mark - Getters
    
    
float AAOsc :: oscAtPhase(double phase) {
    const uint index = static_cast<uint>(phase * 4294967296.0) >> fracBits;
    return currentTable[index];
}




#pragma mark - Generators


float AAOsc :: genTruncate() {
    const uint index = static_cast<uint>(osc->tickInfo->curvPhase >> fracBits);
    
    // advance curPhase with auto-wrap
    osc->tickInfo->curvPhase += osc->tickInfo->increment;
    
    return currentTable[index] * osc->amp;
}


float AAOsc :: genInterpLinear() {
    const uint iBase = static_cast<uint>(osc->tickInfo->curvPhase >> fracBits);
    const uint iNext = iBase + 1;
    
    const double frac = (osc->tickInfo->curvPhase >> fracBits) - iBase;
    const double slope = currentTable[iNext] - currentTable[iBase];
        
    // advance curPhase with auto-wrap
    osc->tickInfo->curvPhase += osc->tickInfo->increment;
    
    return ((frac * slope) + currentTable[iBase]) * osc->amp;
}


float AAOsc :: genInterpCubic() {
    const uint index = static_cast<uint>(osc->tickInfo->curvPhase >> fracBits);
    const double frac = static_cast<double>((osc->tickInfo->curvPhase >> fracBits) - index);
    
    const double ym1 = index > 0 ? currentTable[index-1] : currentTable[osc->tableSize-1];
    const double y0 = currentTable[index];
    const double y1 = currentTable[index+1];
    const double y2 = currentTable[index+2];
                
    // advance curPhase with auto-wrap
    osc->tickInfo->curvPhase += osc->tickInfo->increment;
    
    return ((frac*frac*frac)*(-ym1 - 3.0*y1 + (y2 + 3.0*y0)) / 6.0 +
            (frac*frac)*((ym1 + y1) / 2.0 - y0) +
            frac*(y1 + (-2.0*ym1 - (y2 + 3.0*y0)) / 6.0) + y0) * osc->amp;
}
    
    
void AAOsc :: genSSE_Linear(float *outSignal, uint length) {
//    float phaseArray[length];
//    
//    for (int i=0; i<length; i++) {
//        phaseArray[i] = static_cast<float>(osc->tickInfo->curvPhase >> fracBits);
//        osc->tickInfo->curvPhase += osc->tickInfo->increment;
//    }
//    
//    aA_SSE_vInterpLinear(phaseArray, currentTable, outSignal, length);
//    aA_SSE_vScale(outSignal, osc->amp, outSignal, length);
    
    assert(length <= maxLengthSigVector);
    
    for (int i=0; i<length; i++) {
        phaseArray[i] = static_cast<float>(osc->tickInfo->curvPhase >> fracBits);
        osc->tickInfo->curvPhase += osc->tickInfo->increment;
    }
    
    uint lengthOverrun = length % 4;
    if (lengthOverrun != 0)
        length += (4 - lengthOverrun);
    
    assert(length <= maxLengthSigVector);
    
    aA_SSE_vInterpLinear(phaseArray, currentTable, outSignal, length);
    aA_SSE_vScale(outSignal, osc->amp, outSignal, length);

}
    

   
    

    
    

} // AADsp namespace





















