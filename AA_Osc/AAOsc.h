//  AAOsc.h
//  AA_Osc
//
//  Created by GW on 9/22/12.
//  Copyright (c) 2012 Algorhythm_Audio. All rights reserved.
//  Last Updated: 2013/1/27
//
//  Version: 2.1.131
//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// AAOsc - a wavetable synthesis oscillator class
//
// @Usage
//      - Create a new instance like so:
//          AAOsc *osc = new AAOsc(tableSize, sampleRate);
//          Then call initOsc to configure. See the implementation for more info
//      - You can then set the frequency via setFreq(freq) and set the
//        amplitude via setAmp(amp).
//      - Make sure the host calls changeSampleRate(sampleRate) when it
//        sample rate changes to ensure proper usage.
//      - Use any of the gen functions to get the value and tick the curvPhase forward
//      - To use genSSE_Linear follow the preprocessor requirements in AAIntrinsics.h.  Also call
//        initSigVector(maxLength) where maxLength is a product of bufferSize*kResampleFactor.
//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#ifndef __AA_Osc__AAOsc__
#define __AA_Osc__AAOsc__


#pragma mark Include

#include <iostream>
#include <cmath>

#include "AADsp.h"


namespace AADsp {





#pragma mark - Static and Constant


static const float OscFreqMin = 0.0001;
  


#pragma mark - Enum


#ifndef _OscType_
#define _OscType_

    // all possible wave-table types
    typedef enum {
        tOscType_Sine,
        tOscType_SawUp,
        tOscType_SawDown,
        tOscType_Triangle,
        tOscType_Square,
        tOscType_All, // this builds all the osc types and selects sine to start
        
        tOscType_INVALID = -1
    } OscType;

#endif
    
    






#pragma mark - Struct


// tick position
typedef struct oscTickInfo64 {
    int             increment;
    uint            curvPhase;
} OscTickInfo;


// all the info needed for the oscillator in 64-bit
typedef struct oscDesc64 {
    double          freq;
    double          amp;
    int             type; // conforms to OscType or other subclassed types
    uint            tableSize; // excluding guard points
    double          sizeOverSr;
    OscTickInfo     *tickInfo;
} OscDesc;






#pragma mark - Class

class AAOsc {
    
// instance variables
public:
    
    OscDesc         *osc;
    double          nyquist;
    uint            sr;
    float           *currentTable;
    
    // constents for wraping
    int             fracBits;
    int             fracMask;
    
    
private:
    
    // tables
    float           *tableSine;
    float           *tableSawUp;
    float           *tableSawDn;
    float           *tableTri;
    float           *tableSquare;
    
    // for genSSELinear()
    float           *phaseArray;
    uint            maxLengthSigVector;
        
    
// functions
public:
    
    AAOsc                               (ushort tableSizeInBits, uint sampleRate);
    ~AAOsc();
    
    // Must call init. Builds tables as desired by the user
    // return of 0 is sucess. pass 0 for harms arguments to keep default of 20
    int             initOsc             (const ushort *typesArray, const ushort arraySize,
                                         ushort harmsSawUp=20, ushort harmsSawDn=20, ushort harmsTri=20, ushort harmsSquare=20, ushort harmsSquarePW=20);

    // if using genSSE_Linear, you must call this function
    virtual void    initSigVector       (const uint maxLength);
    
    // oscillator setters
    void            setFreq             (double freq);
    void            setAmp              (double amp);
    void            setSampleRate       (uint sampleRate);
    virtual void    setPhase            (double phase); // assumes value between 0. and 1.
    virtual void    setType             (OscType type);

    
    // oscillator getters
    double          oscFreq()       {return osc->freq;}
    double          oscAmp()        {return osc->amp;}
    int             oscType()       {return osc->type;}
    double          oscNyquist()    {return nyquist;}
    double          oscTableSize()  {return osc->tableSize;}
    virtual double  oscPhase()      {return static_cast<double>(osc->tickInfo->curvPhase / 4294967296.0);}
    
    float           oscAtPhase          (double phase);
    
    // generators
    // Note: the user is responsible to ensure that osc is vailid
    virtual float   genTruncate();
    virtual float   genInterpLinear();
    virtual float   genInterpCubic();

    // if using make sure to call initSigVector() before using
    virtual void    genSSE_Linear       (float *outSignal, uint length);
    
    
    // utility (don't directly use these)
    virtual void    printTable(); // This is just for debugging
    void            normTable           (float *table);
    
    
private:
    
    void            deallocOsc          (OscDesc *ref);
    
    // creation wavetable functions for each wave type
    int             buildWaveSine();
    int             buildWaveSawUp      (ushort harms);
    int             buildWaveSawDown    (ushort harms);
    int             buildWaveTriangle   (ushort harms);
    int             buildWaveSquare     (ushort harms);
    
    // utility
    void            setIncrement();
    
};



} // AADsp namespace

#endif /* defined(__AA_Osc__AAOsc__) */








