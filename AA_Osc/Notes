Purpose (GW) 9/22/12:
        The purpose of this project is to be the home of a multi-functional oscillator
    class for Algorhythm Audio.  It will be built in c++ and should have the following 
    functionallity to start:
    
    - table-based synthesis
    - multi-type:
        - sine
        - saw up
        - saw down
        - triangle
        - square (with adjustable pulse-width)
    - set the table size
    - read using linear, cubic, or quadratic interpolation
    - set the overall amplitude
    - set the frequency
    - can choose between 32 and 64 bit
    
    
    
    
Note to self (GW) 9/22/12:
    - Do I need more than one guard point for cubic/quadratic interpolation?
        
        
Snapshot (GW) 9/23/12: 
    Basics Created


Note to self (GW) 9/23/12:
    - If a function has more than one for loop that it will execute, is it less computationally
    expensive to declare the int i at the beginning and use it for both, or to create a new int
    for each loop, or is it negligable?
    - What values should be expected for phase? 0-360, 0-TW0_PI, 0.0-1.0???
    - Is phase really needed???
    

Answer (GW) 9/24/12:
    - For cubic interpolation it requires two guard points.
    
    
Update (GW) 9/24/12:
        Started inital testing.  I was able to sucessfully play a 440 Hz sine wave with linear 
    interpolation.  After trying the cubic interpolation there was definately a lot of noise.
    The algorithem must be incorrect somewhere.  I checked my code to the books cubic interp
    code and it looks correct.  The best thing might be for me to just look at the actual
    equation and build the algorithem myself.
    
    
Update (GW) 9/25/12:
        I figured out the cubic interp bug.  I was wrong above, the algorithem was off my a 
    missplaced parenthesis (spelling?).  I was also able to build a hermite interpolation
    function.  I compared three interps: linear, cubic, and hermite.  I created a sine wave
    at 10203 Hz, 48000 sample rate, 32-bit, 0.8 amplitude, and a wavetable size of 16384. On
    visual inspection I could see a minor difference between hermite and the other two, but
    the difference between cubic and linear seemed to be negligable.  Although this was only
    a visual inspection, it is unknown what the audible signifigance.  The next phase is to
    test the other wave types.
    
    
Problem (GW) 9/26/12:
        When testing the square wave type I noticed that at lower frequnces, like 10 Hz, there
    were anomalies in the waveform.  For a duration of 5 seconds, and the same parameters as the
    testing above, there would be 2-5 samples that should have been close to 0.8 (being that the
    max amplitude was set to 0.8) yet the sample seemed to be inverted: around -0.8
        I noticed these anomalies for both the cubic and hermite interpolation functions, but I
    didn't see them for the linear interp function.  This could suggest that the problem is 
    inherent with either the index or wrap algorithems.
        The same issue is present for saw up 
        
        
Solved (GW) 9/26/12:
        I found the bug in my code!  I was incorrectly setting the guard points, and the second 
    point was actually 0, which is why I was getting inverted signed samples.  I will do another
    round of testing to ensure my fix worked.
    
    
TODO List (GW) 9/26/12:

    - update code with comments and notes for users
    - add quadratic interpolation function
    - add pulse width functionallity to square wave function
    - add a setPulseWidth function
    - have Christian beta test
    
    
Snapshot (GW) 9/27/12:
    - With 32-bit data type
    
    
Update (GW) 9/28/12:
        The 32-bit struct has been eliminated and the names have changed to exclude 64 for clairity.
    Also I eliminate the abstract data type, it seemed pointless and a waist of memory.  I cleaned
    up all the functions with regards to the single data type (not checking for the data type). Lastly,
    I optomised the truncated and linear interp functions and got rid of the val data type.  The cubic
    and hermite functions could still use some optomizing, but this will be at the cost of readability.
    
    
Working (GW) 9/28/12:
        I finished the update to have all wavetables built.  I tested it out in Logic and I didn't hear
    a single click when changing wave types.
    
    
Commit (GW) 10/17/12:
    - 1.0.0 added setCurvPhase
    
    
Commit (GW) 10/17/12:
    - 1.1.0 using AADsp
    
    
Update (GW) 10/17/12:
        I ammended the constructor method to have the argument for waveType to allow the programmer to
    decide which wave type should be built, or if all of them (which is the default).  This now requires
    that if the programmer wants to build all the wave types and have a wave type that's not the sine
    wave selected, they must call the setWaveType() function.
    
    
Commit (GW) 10/17/12:
    - 1.1.01 Constructor changed


Bug (GW) 11/7/12:
        The genInterpHermite has a local vaiable called y0 that never gets used.  Check the function 
    and see if there's a typo somewhere.
    
    
Commit (GW) 11/7/12:
    - 1.2.0 added second constructor
    
    
Commit (GW) 11/7/12:
    - 1.3.0 fixed potential bug in second constructor tables[] switch
    
    
Commit (GW) 2012/11/19:
    - 1.3.01 fixed initOsc. Need to fix namespace
    
    
Commit (GW) 2012/11/24:
    - 1.4.0 fixed constructor with types array argument
    
    
Commit (GW) 2012/11/29:
    - 1.5.0 optomized
    
    
Commit (GW) 2012/12/4:
    - 1.6.0 removed OscFreqMax, fixed variable types constructor, fixed twice zeroing of tables.
    

Commit (GW) 2012/12/5:
    - 1.7.0 fixed wave type builders.
    
    
Commit (GW) 2012/12/5:
    - 1.8.0 optomized normTable
    
    
Update (GW) 2012/12/6:
        Majory overhaul of code.  I removed the second constructor and require that the user calls initOsc.  This 
    reduces the footprint of the class when less than all the wavetables are required.  It also allows the user to
    set the number of harmonics for each wave type (except for the sine wave for obvious reasons).  
        The build functions now return a value for error checking, as well as the initOsc function.  This has
    increased the construction time, but will allow better debugging, as well as more flexability for the user.


Commit (GW) 2012/12/6:
    - 1.9.0 Big update. See previous note in Notes
    
    
Commit (GW) 2012/12/6:
    - 1.9.1 Removed printf's for speed.


Commit (GW) 2012/12/15:
    - 2.0.0 Converted to float tables and uses AAIntrinsics
    
    
Commit (GW) 2012/12/15:
    - 2.0.1 General cleanup
    
    
Commit (GW) 2012/12/16:
    - 2.0.11 Cleanup and restructure in preparation for subclassing
    
    
Commit (GW) 2012/12/17:
    - 2.0.21 Addition of AAOscPW. Other restructuring.
    
    
Commit (GW) 2012/12/18:
    - 2.0.31 Added linear interp to AAOscPW
    
    
Commit (GW) 2012/12/19:
    - 2.0.41 Added oscPhase() and oscTableSize getters.
    
    
Commit (GW) 2012/12/19:
    - 2.0.51 setType now sets phase to 0
    
    
Update (GW) 2012/12/24:
        I recent came across a better way to handle phase wrapping that doesn't use while loops.  It uses integer overflow
    to automatically wrap the phase.  The next itteration will be an attempt on my part to implement this.  Before I do,
    I will be running this state of the code now thorugh many tests, including: cpu usage, memory allocation, audio quality,
    and init times.  I will also take a snapshot of the code at this point to preserve it.
    
    
Testing (GW) 2012/12/24:

    @Notes
        - All the tests will generate 100 mono oscillators and use sine waves for 5 seconds at 0.8 amplitude at 44.1KHz.
        - The variables will be the wavetable size, interpolation methods (trunc/linear), the frequency (20, 440, 10K),
          and freq automation.
        - I will save the audio files generated for comparison and label them accordingly
        - For quality I will give it a rating of poor/good/great and compare them to like frequencies.
        
        
    @Test1
        @Freq       20
        @Size       512
        @Interp     trunk
        
        @Results
            Init Time   0.062160
            Proc Time   0.326394
            Quality     great
    
    
    @Test2
        @Freq       20
        @Size       1024
        @Interp     trunk
        
        @Results
            Init Time   0.116938
            Proc Time   0.321725
            Quality     great
            
            
    @Test3
        @Freq       20
        @Size       2048
        @Interp     trunk
        
        @Results
            Init Time   0.236289
            Proc Time   0.349653
            Quality     great
            
            
    @Test4
        @Freq       20
        @Size       4096
        @Interp     trunk
        
        @Results
            Init Time   0.478816
            Proc Time   0.382821
            Quality     great
            
            
    @Test5
        @Freq       20
        @Size       8192
        @Interp     trunk
        
        @Results
            Init Time   0.946241
            Proc Time   0.444113
            Quality     great
            
            
    @Test6
        @Freq       20
        @Size       16384
        @Interp     trunk
        
        @Results
            Init Time   1.896476
            Proc Time   0.640070
            Quality     great 
            
            
    @Test7
        @Freq       440
        @Size       512
        @Interp     trunk
        
        @Results
            Init Time   0.059249
            Proc Time   0.348293
            Quality     great 
            
            
    @Test8
        @Freq       440
        @Size       1024
        @Interp     trunk
        
        @Results    
            Init Time   0.120690
            Proc Time   0.327590
            Quality     great 
            
            
    @Test9
        @Freq       440
        @Size       2048
        @Interp     trunk
        
        @Results
            Init Time   0.234573
            Proc Time   0.329820
            Quality     great 
            
            
    @Test10
        @Freq       440
        @Size       4096
        @Interp     trunk
        
        @Results
            Init Time   0.470589
            Proc Time   0.333948
            Quality     great 
            
            
    @Test11
        @Freq       440
        @Size       8192
        @Interp     trunk
        
        @Results
            Init Time   0.965008
            Proc Time   0.380456
            Quality     great 
            
            
    @Test12
        @Freq       440
        @Size       16384
        @Interp     trunk
        
        @Results
            Init Time   1.857200
            Proc Time   0.786233
            Quality     great 

            
    @Test13
        @Freq       10000
        @Size       512
        @Interp     trunk
        
        @Results
            Init Time   0.063225
            Proc Time   0.346157
            Quality 
            
            
    @Test14
        @Freq       10000
        @Size       1024
        @Interp     trunk
        
        @Results
            Init Time   0.121045
            Proc Time   0.357260
            Quality 
            
            
    @Test15
        @Freq       10000
        @Size       2048
        @Interp     trunk
        
        @Results
            Init Time   0.245082
            Proc Time   0.379684
            Quality 
            
            
    @Test16
        @Freq       10000
        @Size       4096
        @Interp     trunk
        
        @Results
            Init Time   0.484499
            Proc Time   0.395568
            Quality 
            
            
    @Test17
        @Freq       10000
        @Size       8192
        @Interp     trunk
        
        @Results
            Init Time   0.966065
            Proc Time   0.405620
            Quality 
            
            
    @Test18
        @Freq       10000
        @Size       16384
        @Interp     trunk
        
        @Results
            Init Time   1.927597
            Proc Time   0.508733
            Quality 
            
            
    @Test19
        @Freq       20
        @Size       512
        @Interp     linear
        
        @Results
            Init Time   0.060896
            Proc Time   0.448889
            Quality     great 
            
            
    @Test20
        @Freq       20
        @Size       1024
        @Interp     linear
        
        @Results
            Init Time   0.119549
            Proc Time   0.443694
            Quality     great 
            
            
    @Test21
        @Freq       20
        @Size       2048
        @Interp     linear
        
        @Results
            Init Time   0.236463
            Proc Time   0.440227
            Quality     great 
            
            
    @Test22
        @Freq       20
        @Size       4096
        @Interp     linear
        
        @Results
            Init Time   0.475661
            Proc Time   0.441485
            Quality     great 
            
            
    @Test23
        @Freq       20
        @Size       8192
        @Interp     linear
        
        @Results
            Init Time   0.947830
            Proc Time   0.443266
            Quality     great 
            
            
    @Test24
        @Freq       20
        @Size       16384
        @Interp     linear
        
        @Results
            Init Time   1.880561
            Proc Time   0.454681
            Quality     great 
            
            
    @Test25
        @Freq       440
        @Size       512
        @Interp     linear
        
        @Results
            Init Time   0.060312
            Proc Time   0.439551
            Quality     great 
            
            
    @Test26
        @Freq       440
        @Size       1024
        @Interp     linear
        
        @Results
            Init Time   0.119456
            Proc Time   0.460397
            Quality     great 
            
            
    @Test27
        @Freq       440
        @Size       2048
        @Interp     linear

        @Results
            Init Time   0.239616
            Proc Time   0.453489
            Quality     great 
            
            
    @Test28
        @Freq       440
        @Size       4096
        @Interp     linear
        
        @Results
            Init Time   0.474731
            Proc Time   0.479586
            Quality     great 
            
            
    @Test29
        @Freq       440
        @Size       8192
        @Interp     linear
        
        @Results
            Init Time   0.955006
            Proc Time   0.632933
            Quality     great 
            
            
    @Test30
        @Freq       440
        @Size       16384
        @Interp     linear
        
        @Results
            Init Time   1.886492
            Proc Time   0.989079
            Quality     great 
            
            
    @Test31
        @Freq       10000
        @Size       512
        @Interp     linear
        
        @Results
            Init Time   0.060816
            Proc Time   0.456897
            Quality 
            
            
    @Test32
        @Freq       10000
        @Size       1024
        @Interp     linear
        
        @Results
            Init Time   0.118231
            Proc Time   0.484159
            Quality 
            
            
    @Test33
        @Freq       10000
        @Size       2048
        @Interp     linear
        
        @Results
            Init Time   0.239422
            Proc Time   0.500907
            Quality 
            
            
    @Test34
        @Freq       10000
        @Size       4096
        @Interp     linear
        
        @Results
            Init Time   0.487993
            Proc Time   0.572303
            Quality 
            
            
    @Test35
        @Freq       10000
        @Size       8192
        @Interp     linear
        
        @Results
            Init Time   0.974018
            Proc Time   0.799086
            Quality 
            
            
    @Test36
        @Freq       10000
        @Size       16384
        @Interp     linear
        
        @Results
            Init Time   1.934559
            Proc Time   1.239805
            Quality 
            
            
Snapshot (GW) 2012/12/24:
    2.0.51
        - prior to phase wrap restructure


Notes: (GW) 2013/1/9:
        The init times are about the same while the proc times improved by about %20-25 for the test that took the longest, Test36.
        
        
Commit (GW) 2013/1/9:
    - 2.1.0 Now includes integer overflow wrap.  Improved proc speed by %25. AAOscPw is inactive for now.
    
    
Commit (GW) 2013/1/10:
    - 2.1.1 AAOscPW now works.  The setPW needs parameter smoothing
    
    
Commit (GW) 2013/1/12:
    - 2.1.11 Fixed tables init to NULL in the declaration.


Commit (GW) 2013/1/12:
    - 2.1.21 Uses updated AADsp version 1.0.102
    
    
Commit (GW) 2013/1/12:
    - 2.1.31 Small optimizations


Commit (GW) 2013/1/12:
    - 2.1.41 Small optimizations to AAOscPW


Commit (GW) 2013/1/13:
    - 2.1.51 Changed malloc tables to mallocAlligned.
    
    
Commit (GW) 2013/1/13:
    - 2.1.61 Added AABasic.  Uses aa_fastSine and cos.
    
    
Commit (GW) 2013/1/14:
    - 2.1.71 Removed AABasic and the fast trig functions.


Commit (GW) 2013/1/17:
    - 2.1.81 Added sse linear interp functions.  Need to be tested!
    
    
Commit (GW) 2013/1/24: 
    - 2.1.91 General cleanup and documentation updates


Commit (GW) 2013/1/26:
    - 2.1.101 Added windows functions and members
    
    
Commit (GW) 2013/1/27:
    - 2.1.111 Added new AAIntrinsics file.


Commit (GW) 2013/1/27:
    - 2.1.121 Updated genSSELinear() and removed windows #ifdefs


Commit (GW) 2013/1/27:
    - 2/1/131 Updated AAOscPW with sse support in 2.1.121






















